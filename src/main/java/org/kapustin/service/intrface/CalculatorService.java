package org.kapustin.service.intrface;

import org.kapustin.dto.CalculatorHolderDto;
import org.kapustin.exceptions.ZeroDivideException;

public interface CalculatorService {
    int summary(int num1, int num2);

    int diff(int num1, int num2);

    int multiply(int num1, int num2);

    int divide(int num1, int num2) throws ZeroDivideException;

    int calculateOperation(CalculatorHolderDto calculatorHolderDto) throws ZeroDivideException;

    int calculatorService() throws ZeroDivideException;
}