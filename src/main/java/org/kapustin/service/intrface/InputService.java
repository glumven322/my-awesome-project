package org.kapustin.service.intrface;

import org.kapustin.dto.CalculatorHolderDto;

public interface InputService {

    CalculatorHolderDto getCalculatorHolderDto();
}
