package org.kapustin.dto;

public class CalculatorHolderDto {
    private int num1;
    private int num2;
    private char operation;

    public CalculatorHolderDto(int num1, int num2, char operation) {
        this.num1 = num1;
        this.num2 = num2;
        this.operation = operation;
    }

    public int getNum1() {
        return num1;
    }

    public int getNum2() {
        return num2;
    }

    public char getOperation() {
        return operation;
    }

}
