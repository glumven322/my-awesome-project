package org.kapustin;

import org.kapustin.exceptions.ZeroDivideException;
import org.kapustin.service.implementation.CalculatorServiceImpl;

public class Application {
    private static final CalculatorServiceImpl Calculator = new CalculatorServiceImpl();

    public static void main(String[] args) throws ZeroDivideException {
        Calculator.calculatorService();
    }
}

